// console.log("Heloo");

// [SECTION] While Loop

// If the condition evaluates to true, the statements inside the code blcok will executed

/*
	Syntax 
	while (expression/condition){
		statement
	}
*/

// While the value of count is not equal to 0


let count = 5;

while(count !== 0){
	console.log("While: " + count);

	// iteration - it increases the value of count after every iteration to stop the loop when it reaches 5

	// ++ - increment, -- decrement

	count --;
}

// ====================================================================================

// [SECTION] Do While Loop

// A do-while loop works a  lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

/*
	Syntax:

	do {
		statement
	} while (expression/condition)
*/


let number = Number(prompt("Give me a number"));

do {

	console.log("Do While: " + number);
	number += 1;

} while (number < 10)

// [SECTION] For Loops

// A for loop is more flexible than while and do-while loops.

/*
	Syntax
	for (initialization: )
*/